#include <stdlib.h>
#include <stdio.h>
#define LEN 30

int dim=0;

struct s_persona{
  char nome[LEN];
  int anni;
  float peso;
  int altezza;
};
typedef struct s_persona persona;

void stampa(persona*);
int confronta_anni(const void *, const void *);
int confronta_peso(const void *, const void *);
int confronta_altezza(const void *, const void*);

int main(){
  int i=0;
  persona *elenco;

  freopen("dati.dat","r",stdin);
  scanf("%d\n",&dim);
  elenco=(persona*)malloc(dim*sizeof(persona));

  printf("Dimensione: %d\n",dim);

  for(i=0; i<dim; i++){
    scanf("%s %d %f %d\n",elenco[i].nome,&(elenco[i].anni),&(elenco[i].peso),&(elenco[i].altezza));
  }

  stampa(elenco);

  qsort(elenco,dim,sizeof(persona),confronta_anni);
  printf("\nOrdinata per anni (ordinamento crescente): \n");
  stampa(elenco);

  qsort(elenco,dim,sizeof(persona),confronta_peso);
  printf("\nOrdinata per peso (ordinamento decrescente): \n");
  stampa(elenco);

  qsort(elenco,dim,sizeof(persona),confronta_altezza);
  printf("\nOrdinata per altezza  (ordinamento crescente): \n");
  stampa(elenco);

}

int confronta_anni(const void *a, const void *b){
  persona *uno=(persona*)a;
  persona *due=(persona*)b;
  return uno->anni - due->anni;
}
int confronta_peso(const void *a, const void *b){
  persona *uno=(persona*)a;
  persona *due=(persona*)b;
  return due->peso - uno->peso;
}
int confronta_altezza(const void *a, const void *b){
  persona *uno=(persona*)a;
  persona *due=(persona*)b;
  return uno->altezza - due->altezza;
}

void stampa(persona* elenco){
  int i=0;
  for(i=0; i<dim; i++){
    printf("%s\t%d\t%.2f\t%d\n",elenco[i].nome,elenco[i].anni,elenco[i].peso,elenco[i].altezza);
  }
}
